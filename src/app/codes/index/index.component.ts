import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-codes',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    rows;
    codes = [];

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (params) => {
            this.company_id = params.id;
            this.codes = await this.restangular.one('companies', this.company_id).all('codes').getList().toPromise();
            this.rows = this.codes;
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.codes.filter(function(d) {
            return d.sender.email && d.sender.email.toLowerCase().indexOf(val) !== -1 ||
                    d.sender.name && d.sender.name.toLowerCase().indexOf(val) !== -1 ||
                    d.recipient && d.recipient.email && d.recipient.email.toLowerCase().indexOf(val) !== -1 ||
                    d.recipient && d.recipient.name && d.recipient.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

}
