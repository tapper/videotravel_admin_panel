import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

    public form: FormGroup;
    public wrongCredentials: boolean = false;
    public error: string = '';

    constructor(private fb: FormBuilder, private router: Router, public restangular: Restangular) {}

    ngOnInit() {
        this.form = this.fb.group({
            uname: [null, Validators.compose([Validators.required])],
            password: [null, Validators.compose([Validators.required])]
        });
    }

    async onSubmit() {

        this.wrongCredentials = false;

        try {
            let response = await this.restangular.all('login').customPOST({login: this.form.value.uname, password: this.form.value.password}).toPromise();
            if (response.company_id){
                localStorage.setItem('id', response.id);
                localStorage.setItem('company_id', response.company_id);
                localStorage.setItem('type', 'manager');
                this.router.navigate(['/companies/' + response.company_id + '/managers/' + response.id])
            } else {
                localStorage.setItem('id', response.id);
                localStorage.setItem('type', 'admin');
                this.router.navigate(['/'])
            }
        } catch (err){
            console.log(err);
            this.wrongCredentials = true;
            if (err && err.data && err.data.error && err.data.error.message){
                this.error = err.data.error.message;
            }
        }

    }

}
