import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-messages',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    itemToDelete: any;
    messages;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('messages').getList().toPromise();
        this.messages = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.messages.filter(function(d) {
            return d.user.name && d.user.name.toLowerCase().indexOf(val) !== -1 ||
                d.user.email && d.user.email.toLowerCase().indexOf(val) !== -1 ||
                d.company.title && d.company.title.toLowerCase().indexOf(val) !== -1 ||
                !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('messages').getList().toPromise();
        this.messages = this.rows;
    }

    openDeleteModal(content, region) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = region;
    }

}
