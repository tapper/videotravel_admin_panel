import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    public loading = false;
    image = {path: null, file: null};
    errors: Array<any> = [];
    company_id: number = 0;
    companies;
    explanation: boolean = false;
    language: string = 'he';

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer) {}

    async ngOnInit() {
        this.companies = await this.restangular.all('companies').getList().toPromise();
    }

    onFileChange(event) {
        let file = event.target.files[0];
        let reader = new FileReader();
        reader.onload = ev => {
            this.image.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
            this.image.file =  file;
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (!this.explanation && this.company_id === 0){
            this.errors.push({message: 'Please choose company or change the banner type'});
            return;
        }

        if (this.language === ''){
            this.errors.push({message: 'Please choose language'});
            return;
        }

        this.loading = true;

        let banner = this.restangular.restangularizeElement('', {}, 'banners');
        banner.type = this.explanation ? 'explanation' : 'ad';
        banner.company_id = this.explanation ? null : this.company_id;
        banner.language = this.language;
        let response = await banner.save().toPromise();

        let fd = new FormData();
        fd.append('entity_id', response.id);
        fd.append('entity_type', 'banner');
        fd.append('media_key', 'image');
        fd.append('image', this.image.file);

        await this.restangular.all('files').customPOST(fd).toPromise();

        this.loading = false;
        this.router.navigate(['/banners']);
    }


}

