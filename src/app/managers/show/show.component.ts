import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-managers',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

    manager: any;
    company_id: any;
    countries;
    points;
    phone = '';
    sent: boolean = false;
    error: boolean = false;
    form: FormGroup = this.fb.group({
        phone: new FormControl('', Validators.required),
        country: new FormControl('IL', Validators.required),
        language: new FormControl('he', Validators.required),
        quantity: new FormControl(null, Validators.required),
    });

    constructor(public restangular: Restangular, public fb: FormBuilder) {}

    async ngOnInit() {
        let id = window.localStorage.getItem('id');
        this.company_id = window.localStorage.getItem('company_id');
        this.manager = await this.restangular.one('companies', this.company_id).one('managers', id).get().toPromise();
        this.countries = await this.restangular.one('companies', this.company_id).one('managers', id).customGET('countries').toPromise();
        this.points = await this.restangular.all('points').getList().toPromise();
    }

    async onSubmit () {
        try {
            let payload:any = {};
            payload.manager_id = this.manager.id;
            payload.phone = this.form.value.phone;
            payload.language = this.form.value.language;
            payload.country = this.form.value.country;
            payload.quantity = this.form.value.quantity;

            let response = await this.restangular.one('companies', this.company_id).all('codes').customPOST(payload).toPromise();
            this.phone = response.phone;
            this.sent = true;
            this.form = this.fb.group({
                phone: new FormControl('', Validators.required),
                country: new FormControl('IL', Validators.required),
                language: new FormControl('he', Validators.required),
                quantity: new FormControl(null, Validators.required),
            });
        } catch (err) {
            this.error = true;
            console.log(err);
        }
    }

    closeAlert () {
        this.sent = false;
    }


}
