import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    public loading = false;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
        owner: new FormControl(false, Validators.required),
    });
    errors: Array<any> = [];
    company_id: number;

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => this.company_id = data['id']);
    }

    async onSubmit() {

        this.errors = [];

        this.loading = true;
        let manager = this.restangular.restangularizeElement(this.restangular.one('companies', this.company_id), {}, 'managers');
        manager.name = this.form.value.name;
        manager.email = this.form.value.email;
        manager.owner = this.form.value.owner;

        try {
            await manager.save().toPromise();
            this.router.navigate(['/companies/' + this.company_id + '/managers']);
        } catch (err){
            err = err.json();
            this.errors.push({message: err.error.message});
        } finally {
            this.loading = false;
        }
    }
}

