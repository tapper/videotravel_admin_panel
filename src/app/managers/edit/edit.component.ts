import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    public loading = false;
    manager_id;
    manager;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
        owner: new FormControl(false, Validators.required),
    });
    errors: Array<any> = [];
    company_id: number;

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.parent.params.subscribe(parentParams => {
            this.company_id = parentParams.id;
            this.activatedRoute.params.subscribe(async (data) => {
                this.manager_id = data['id'];
                this.manager = await this.restangular.one('companies', this.company_id).one('managers', this.manager_id).get().toPromise();
                this.form.setValue({
                    name: this.manager.name,
                    email: this.manager.email,
                    owner: this.manager.owner
                });
            });
        });
    }

    async onSubmit() {

        this.errors = [];

        this.loading = true;
        this.manager.name = this.form.value.name;
        this.manager.email = this.form.value.email;
        this.manager.owner = this.form.value.owner;

        try {
            await this.manager.save().toPromise();
            this.router.navigate(['/companies/' + this.company_id + '/managers']);
        } catch (err){
            err = err.json();
            this.errors.push({message: err.error.message});
        } finally {
            this.loading = false;
        }
    }
}



