import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-managers',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    company;
    deleteModal: any;
    messageModal: any;
    customMessage: string = '';
    itemToDelete: any;
    managers;
    rows: Array<any>;
    errors = [];
    manager: any;

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data['id'];
            this.company = await this.restangular.one('companies', this.company_id).get().toPromise();
            this.rows = await this.restangular.one('companies', this.company_id).all('managers').getList().toPromise();
            this.managers = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.managers.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || d.email && d.email.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.one('companies', this.company_id).all('managers').getList().toPromise();
        this.managers = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    openMessageModal(content, item){
        this.messageModal = this.modalService.open(content);
        this.manager = item;
    }

    async resendCredentials () {
        this.errors = [];
        await this.restangular.one('companies', this.company_id).one('managers', this.manager.id).all('credentials').customPOST({message: this.customMessage}).toPromise();
        this.errors.push({message: "Email and new password are successfully sent to email " + this.manager.email});
        this.messageModal.close();
        this.customMessage = '';
    }
}
