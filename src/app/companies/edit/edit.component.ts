import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Restangular} from "ngx-restangular";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MapsAPILoader} from '@agm/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    id: number;
    company: any;
    public loading = false;
    regions = [];
    company_categories = [];
    formatted_address = [];
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required),
        address: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        description_en: new FormControl('', Validators.required),
        description_he: new FormControl('', Validators.required),
        region: new FormControl(null, Validators.required),
        company_category: new FormControl(null, Validators.required),
        company_subcategory: new FormControl(null, Validators.required),
        weight: new FormControl('')
    });
    logo = {path: null, file: null};
    main_video = {path: null, file: null};
    errors: Array<any> = [];

    constructor(public restangular: Restangular,
                private activatedRoute: ActivatedRoute,
                public fb: FormBuilder,
                public loader: MapsAPILoader,
                private router: Router,
                public sanitizer: DomSanitizer) {}

    async ngOnInit() {

        this.regions = await this.restangular.all('regions').getList().toPromise();
        this.company_categories = await this.restangular.all('company_categories').getList().toPromise();

        this.activatedRoute.params.subscribe(async (data) => {
            this.company = await this.restangular.one('companies', data['id']).get().toPromise();
            this.form.setValue({
                title_en: this.company.title_en,
                title_he: this.company.title_he,
                address: this.company.address,
                phone: this.company.phone,
                description_en: this.company.description_en,
                description_he: this.company.description_he,
                region: this.company.region_id,
                company_category: this.company.company_category_id,
                company_subcategory: this.company.company_subcategory_id,
                weight: this.company.weight === null ? '' : this.company.weight
            })
        });

        await this.loader.load();

        let address = document.getElementById('address').getElementsByTagName('input')[0];
        let autocomplete = new google.maps.places.Autocomplete(address);

        autocomplete.addListener("place_changed", () => {

            let place = autocomplete.getPlace();
            let countryComponent = '';

            for (let item of place.address_components){
                if (item.types[0] === 'country')
                    countryComponent = item.short_name;
            }
    
            //shay added
            if(countryComponent == "")
            {
                countryComponent =  "IL";
                console.log("ISRAEL")
            }
            
            
            this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
            console.log(this.formatted_address);

        })
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            } else {
                this.main_video.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.main_video.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }


    async onSubmit() {

        this.errors = [];

        if (!this.formatted_address.length){
            this.errors.push({message: "Please check address once again!"});
            return;
        }

        for (let component in this.formatted_address){
            if (this.formatted_address[component] == undefined || this.formatted_address[component] == ''){
                this.errors.push({message: "Please check address once again!"});
                switch (component){
                    case '0':
                        this.errors.push({message: "Address couldn't be found by Google!"});
                        return;
                    case '1':
                        this.errors.push({message: "This place has no country code! Please check once again the list of supposed addresses"});
                        return;
                    case '2':
                        this.errors.push({message: "Latitude couldn't be found by Google!"});
                        return;
                    case '3':
                        this.errors.push({message: "Longitude couldn't be found by Google!"});
                        return;
                    default:
                        return;

                }
            }

        }

        this.loading = true;
        this.company.title_en = this.form.value.title_en;
        this.company.title_he = this.form.value.title_he;
        this.company.company_subcategory_id = this.form.value.company_subcategory;
        this.company.region_id = this.form.value.region;
        this.company.phone = this.form.value.phone;
        this.company.address = this.formatted_address[0];
        this.company.country = this.formatted_address[1];
        this.company.lat = this.formatted_address[2];
        this.company.lng = this.formatted_address[3];
        this.company.description_en = this.form.value.description_en;
        this.company.description_he = this.form.value.description_he;
        this.company.weight = this.form.value.weight;

        let response = await this.company.patch().toPromise();    // TODO: process errors

        if (this.logo.file !== null){
            let fdLogo = new FormData();
            fdLogo.append('entity_id', response.id);
            fdLogo.append('entity_type', 'company');
            fdLogo.append('media_key', 'logo');
            fdLogo.append('logo', this.logo.file);

            await this.restangular.all('files').customPOST(fdLogo).toPromise();
        }

        if (this.main_video.file !== null){
            let fdVideo = new FormData();
            fdVideo.append('entity_id', response.id);
            fdVideo.append('entity_type', 'company');
            fdVideo.append('media_key', 'main_video');
            fdVideo.append('main_video', this.main_video.file);

            await this.restangular.all('files').customPOST(fdVideo).toPromise();
        }

        this.loading = false;
        this.router.navigate(['/companies']);

    }


}
