import {ChangeDetectionStrategy, Component, NgZone, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {MapsAPILoader} from '@agm/core';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    public loading = false;
    regions = [];
    company_categories = [];
    formatted_address = [];
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required),
        address: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        description_en: new FormControl('', Validators.required),
        description_he: new FormControl('', Validators.required),
        region: new FormControl(null, Validators.required),
        company_category: new FormControl(null, Validators.required),
        company_subcategory: new FormControl(null, Validators.required),
        weight: new FormControl(''),
    });
    logo = {path: null, file: null};
    main_video = {path: null, file: null};
    errors: Array<any> = [];


    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                public loader: MapsAPILoader,
                private router: Router,
                private sanitizer: DomSanitizer) {}

    async ngOnInit() {
        this.regions = await this.restangular.all('regions').getList().toPromise();
        this.company_categories = await this.restangular.all('company_categories').getList().toPromise();

        await this.loader.load();

        let address = document.getElementById('address').getElementsByTagName('input')[0];
        let autocomplete = new google.maps.places.Autocomplete(address);

        autocomplete.addListener("place_changed", () => {

            let place = autocomplete.getPlace();
            let countryComponent = '';

            for (let item of place.address_components){
                if (item.types[0] === 'country')
                    countryComponent = item.short_name;
            }
    
            //shay added
            if(countryComponent == "")
                countryComponent =  "IL";
            
            this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
            console.log(this.formatted_address);

        })
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            } else {
                this.main_video.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.main_video.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null){
            this.errors.push({message: "Logo is empty!"});
            return;
        }

        if (this.main_video.file === null){
            this.errors.push({message: "Video is empty!"});
            return;
        }

        if (!this.formatted_address.length){
            this.errors.push({message: "Please check address once again!"});
            return;
        } 

        for (let component in this.formatted_address){
            if (this.formatted_address[component] == undefined || this.formatted_address[component] == ''){
                this.errors.push({message: "Please check address once again!"});
                switch (component){
                    case '0':
                        this.errors.push({message: "Address couldn't be found by Google!"});
                        return;
                    case '1':
                        this.errors.push({message: "This place has no country code! Please check once again the list of supposed addresses"});
                        return;
                    case '2':
                        this.errors.push({message: "Latitude couldn't be found by Google!"});
                        return;
                    case '3':
                        this.errors.push({message: "Longitude couldn't be found by Google!"});
                        return;
                    default:
                        return;

                }
            }

        }

        this.loading = true;
        let company = this.restangular.restangularizeElement('', {}, 'companies');
        company.title_en = this.form.value.title_en;
        company.title_he = this.form.value.title_he;
        company.company_subcategory_id = this.form.value.company_subcategory;
        company.region_id = this.form.value.region;
        company.phone = this.form.value.phone;
        company.address = this.formatted_address[0];
        company.country = this.formatted_address[1];
        company.lat = this.formatted_address[2];
        company.lng = this.formatted_address[3];
        company.description_en = this.form.value.description_en;
        company.description_he = this.form.value.description_he;
        company.weight = this.form.value.weight;

        let response = await company.save().toPromise();    // TODO: process errors

        let fdLogo = new FormData();
        fdLogo.append('entity_id', response.id);
        fdLogo.append('entity_type', 'company');
        fdLogo.append('media_key', 'logo');
        fdLogo.append('logo', this.logo.file);

        await this.restangular.all('files').customPOST(fdLogo).toPromise();

        let fdVideo = new FormData();
        fdVideo.append('entity_id', response.id);
        fdVideo.append('entity_type', 'company');
        fdVideo.append('media_key', 'main_video');
        fdVideo.append('main_video', this.main_video.file);

        await this.restangular.all('files').customPOST(fdVideo).toPromise();

        this.loading = false;
        this.router.navigate(['/companies']);
    }


}