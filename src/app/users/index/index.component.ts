import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-users',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    sendPointsModal: any;
    sendPushModal: any;
    itemToDelete: any;
    users;
    rows: Array<any>;
    quantity: number;
    push = '';
    userIdToProcess: number = 0;
    sent: boolean = false;

    constructor(public restangular: Restangular, private modalService: NgbModal) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.users.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 ||
                d.email && d.email.toLowerCase().indexOf(val) !== -1 ||
                d.phone && d.phone.toLowerCase().indexOf(val) !== -1 ||
                !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    openSendPointsModal(content, user) {
        this.sendPointsModal = this.modalService.open(content);
        this.userIdToProcess = user.id;
    }

    openSendPushModal(content, user) {
        this.sent = false;
        this.sendPushModal = this.modalService.open(content);
        if (user){
            this.userIdToProcess = user.id;
        }
    }

    async sendPoints () {
        await this.restangular.one('users', this.userIdToProcess).customPOST({points: this.quantity}, 'points').toPromise();
        this.sendPointsModal.close();
        this.rows = await this.restangular.all('users').getList().toPromise();
        this.users = this.rows;
    }

    async sendPush () {
        if (this.push !== ''){
            await this.restangular.all('users').customPOST({text: this.push, user_id: this.userIdToProcess}, 'push').toPromise();
            this.sendPushModal.close();
            this.push = '';
            this.sent = true;
        }
    }

}
