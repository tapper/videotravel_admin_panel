import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-guides',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    guides;
    rows: Array<any>;

    constructor(public restangular: Restangular, public activatedRoute: ActivatedRoute, public sanitizer: DomSanitizer) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('guides').getList().toPromise();
        for (let row of this.rows){
            row.link = this.sanitizer.bypassSecurityTrustResourceUrl(row.link);
        }

        this.guides = this.rows;
    }

}
