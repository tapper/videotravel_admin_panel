import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    public loading = false;
    errors: Array<any> = [];
    guide;
    form: FormGroup = this.fb.group({
        link: new FormControl('', Validators.required),
        language: new FormControl('', Validators.required),
        purpose: new FormControl('', Validators.required),
    });
    sanitizedLink: any = '';

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.guide = await this.restangular.one('guides', data.id).get().toPromise();
            this.form.setValue({
                link: this.guide.link,
                language: this.guide.language,
                purpose: this.guide.purpose,
            });
            this.sanitizedLink = this.sanitizer.bypassSecurityTrustResourceUrl(this.guide.link);
        });
        this.onChanges();
    }

    onChanges () {
        this.form.get('link').valueChanges.subscribe(val => {
            this.sanitizedLink = this.sanitizer.bypassSecurityTrustResourceUrl(this.form.value.link);
        });
    }

    async onSubmit() {

        this.errors = [];

        if (this.form.value.link.indexOf('embed') < 0){
            this.errors.push({message: 'The link is wrong and the application couldn\'t display it.'});
            return;
        }

        this.loading = true;

        this.guide.link = this.form.value.link;
        this.guide.language = this.form.value.language;
        this.guide.purpose = this.form.value.purpose;
        await this.guide.patch().toPromise();

        this.loading = false;
        this.router.navigate(['/guides']);
    }


}
