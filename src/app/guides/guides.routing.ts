import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {EditComponent} from './edit/edit.component';

export const GuidesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Guides'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Guide'},
        }
    ]
}];
