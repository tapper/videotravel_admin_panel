import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-regions',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    createModal: any;
    editMode: boolean = false;
    editRow: any = null;
    regionToDelete: any;
    fieldsToEdit = {title_en: '', title_he: ''};
    regions;
    rows: Array<any>;
    form: FormGroup = this.fb.group({
        title_en: new FormControl('', Validators.required),
        title_he: new FormControl('', Validators.required)
    });

    constructor(public restangular: Restangular, private modalService: NgbModal, public fb: FormBuilder) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('regions').getList().toPromise();
        this.regions = this.rows;
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.regions.filter(function(d) {
            return d.title_en && d.title_en.toLowerCase().indexOf(val) !== -1 || d.title_he && d.title_he.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    switchToEditMode (item) {
        this.editMode = true;
        this.editRow = item;
        this.fieldsToEdit.title_en = item.title_en;
        this.fieldsToEdit.title_he = item.title_he;
    }

    async editItem () {
        this.editRow.title_en = this.fieldsToEdit.title_en;
        this.editRow.title_he = this.fieldsToEdit.title_he;
        await this.editRow.patch().toPromise();
        this.rows = await this.restangular.all('regions').getList().toPromise();
        this.regions = this.rows;
        this.cancelEdit();
    }

    cancelEdit () {
        this.editMode = false;
        this.editRow = null;
    }

    async deleteItem(){
        await this.regionToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('regions').getList().toPromise();
        this.regions = this.rows;
    }

    openDeleteModal(content, region) {
        this.deleteModal = this.modalService.open(content);
        this.regionToDelete = region;
    }

    openCreateModal (content) {
        this.createModal = this.modalService.open(content);
    }

    async createItem (){
        let region = this.restangular.restangularizeElement('', {title_en: this.form.value.title_en, title_he: this.form.value.title_he}, 'regions');
        await region.save().toPromise();
        this.createModal.close();
        this.rows = await this.restangular.all('regions').getList().toPromise();
        this.regions = this.rows;
    }

}
