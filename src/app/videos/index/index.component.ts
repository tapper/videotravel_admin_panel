import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-videos',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    deleteModal: any;
    itemToDelete: any;
    rows;
    media;
    errors = [];
    search: any = {category: '0', text: ''};

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data['id'] || '';
            this.rows = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
            this.media = this.rows;
        });
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
        this.media = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = this.restangular.restangularizeElement('', item, 'files');
    }

    async changeStatus (item, status) {
        await this.restangular.one('files', item.id).customPOST({status: status, user_id: item.user.id}, 'status').toPromise();
        this.rows = await this.restangular.all('files').customGET('', {company_id: this.company_id}).toPromise();
        this.media = this.rows;
    }

    updateFilter() {

        let temp = [];

        if (this.search.category == 0 && this.search.text == ''){
            temp = this.media;
        }

        if (this.search.category != 0){
            temp = this.media.filter((d) => {
                return d.company.company_category_id && d.company.company_category_id == this.search.category;
            });
        }

        if (this.search.text != ''){
            if (this.search.category != 0){
                temp = temp.filter((d) => {
                    return d.company.title && d.company.title.toLowerCase().indexOf(this.search.text) !== -1;
                });
            } else {
                temp = this.media.filter((d) => {
                    return d.company.title && d.company.title.toLowerCase().indexOf(this.search.text) !== -1;
                });
            }
        }

        this.rows = temp;
    }

}
