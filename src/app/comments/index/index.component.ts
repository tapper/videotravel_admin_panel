import {Component, OnInit} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-comments',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    company_id: number;
    video_id: number;
    company;
    deleteModal: any;
    itemToDelete: any;
    rows;
    errors = [];
    comments = [];

    constructor(public restangular: Restangular, private modalService: NgbModal, public activatedRoute: ActivatedRoute) {}

    async ngOnInit() {
        this.activatedRoute.parent.params.subscribe(parentParams => {
            this.company_id = parentParams.id;
            this.activatedRoute.params.subscribe(async (params) => {
                this.video_id = params.id;
                this.comments = await this.restangular.one('files', this.video_id).all('comments').getList().toPromise();
                this.rows = this.comments;
            })
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.comments.filter(function(d) {
            return d.user.name && d.user.name.toLowerCase().indexOf(val) !== -1 || d.content && d.content.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.comments = await this.restangular.one('files', this.video_id).all('comments').getList().toPromise();
        this.rows = this.comments;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

}
