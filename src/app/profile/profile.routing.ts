import {Routes} from '@angular/router';
import {IndexComponent} from "./profile.component";

export const ProfileRoute: Routes = [{
    path: '',
    children: [{
        path: '',
        component: IndexComponent,
        data: {heading: 'Profile'},
    }
    ]
}];
