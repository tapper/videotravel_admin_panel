import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {CategoriesRoutes} from "./categories.routing";
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {IndexComponent} from "./index/index.component";

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { TreeModule } from 'angular-tree-component';
import { CustomFormsModule } from 'ng2-validation';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';
import {LoadingModule} from 'ngx-loading';

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(CategoriesRoutes),
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbProgressbarModule,
        CustomFormsModule,
        TreeModule,
        FileUploadModule,
        LoadingModule,
        TextMaskModule],
    declarations: [IndexComponent]
})

export class CategoriesModule {}