import {Component, OnInit} from '@angular/core';
import {Restangular} from "ngx-restangular";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-points',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    deleteModal: any;
    createModal: any;
    itemToDelete: any;
    points;
    quantity: number;
    rows: Array<any>;

    constructor(public restangular: Restangular, private modalService: NgbModal) {}

    async ngOnInit() {
        this.rows = await this.restangular.all('points').getList().toPromise();
        this.points = this.rows;
    }

    async deleteItem(){
        await this.itemToDelete.remove().toPromise();
        this.deleteModal.close();
        this.rows = await this.restangular.all('points').getList().toPromise();
        this.points = this.rows;
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    openCreateModal (content) {
        this.createModal = this.modalService.open(content);
    }

    async createItem (){
        if (this.quantity !== 0){
            let point = this.restangular.restangularizeElement('', {quantity: this.quantity}, 'points');
            await point.save().toPromise();
            this.createModal.close();
            this.rows = await this.restangular.all('points').getList().toPromise();
            this.points = this.rows;
        }
    }

}
