import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Restangular} from 'ngx-restangular';
import {ActivatedRoute, Router} from '@angular/router';
import * as Quill from 'quill';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    public loading = false;
    faq;
    form: FormGroup = this.fb.group({
        question_en: new FormControl('', Validators.required),
        question_he: new FormControl('', Validators.required),
        answer_en: new FormControl('', Validators.required),
        answer_he: new FormControl('', Validators.required),
    });
    errors: Array<any> = [];

    constructor(public restangular: Restangular,
                public fb: FormBuilder,
                private router: Router,
                public activatedRoute: ActivatedRoute) {}

    ngOnInit() {

        this.activatedRoute.params.subscribe(async (data) => {
            this.faq = await this.restangular.one('faq', data['id']).get().toPromise();
            this.form.setValue({
                question_en: this.faq.question_en,
                question_he: this.faq.question_he,
                answer_en: '',
                answer_he: '',
            });

            const quill_en = new Quill('#editor-container-en', {
                modules: {toolbar: {container: '#toolbar-toolbar-en'}},
                theme: 'snow'
            });

            quill_en.clipboard.dangerouslyPasteHTML(this.faq.answer_en);

            const quill_he = new Quill('#editor-container-he', {
                modules: {toolbar: {container: '#toolbar-toolbar-he'}},
                theme: 'snow'
            });

            quill_he.clipboard.dangerouslyPasteHTML(this.faq.answer_he);

            quill_en.on('text-change', () => {
                this.form.controls.answer_en.setValue(quill_en.root.innerHTML);
            });

            quill_he.on('text-change', () => {
                this.form.controls.answer_he.setValue(quill_he.root.innerHTML);
            });
        });
    }

    async onSubmit() {

        this.faq.question_en = this.form.value.question_en;
        this.faq.question_he = this.form.value.question_he;
        this.faq.answer_en = this.form.value.answer_en;
        this.faq.answer_he = this.form.value.answer_he;

        await this.faq.patch().toPromise();
        this.router.navigate(['/faq']);

    }


}
