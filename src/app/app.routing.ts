import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {AuthGuard} from './_guards/auth.guard';

export const AppRoutes: Routes = [{
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'profile',
            loadChildren: './profile/profile.module#ProfileModule'
        },
        {
            path: 'users',
            loadChildren: './users/users.module#UsersModule'
        },
        {
            path: 'companies',
            loadChildren: './companies/companies.module#CompaniesModule'
        },
        {
            path: 'companies/:id/coupons',
            loadChildren: './coupons/coupons.module#CouponsModule'
        },
        {
            path: 'companies/:id/codes',
            loadChildren: './codes/codes.module#CodesModule'
        },
        {
            path: 'companies/:id/purchases',
            loadChildren: './purchases/purchases.module#PurchasesModule'
        },
        {
            path: 'companies/:id/managers',
            loadChildren: './managers/managers.module#ManagersModule'
        },
        {
            path: 'companies/:id/videos',
            loadChildren: './videos/videos.module#VideosModule'
        },
        {
            path: 'videos',
            loadChildren: './videos/videos.module#VideosModule'
        },
        {
            path: 'companies/:id/videos/:id/comments',
            loadChildren: './comments/comments.module#CommentsModule'
        },
        {
            path: 'regions',
            loadChildren: './regions/regions.module#RegionsModule'
        },
        {
            path: 'categories/:id',
            loadChildren: './categories/categories.module#CategoriesModule'
        },
        {
            path: 'faq',
            loadChildren: './faq/faq.module#FaqModule'
        },
        {
            path: 'banners',
            loadChildren: './banners/banners.module#BannersModule'
        },
        {
            path: 'guides',
            loadChildren: './guides/guides.module#GuidesModule'
        },
        {
            path: 'leads',
            loadChildren: './leads/leads.module#LeadsModule'
        },
        {
            path: 'messages',
            loadChildren: './messages/messages.module#MessagesModule'
        },
        {
            path: 'points',
            loadChildren: './points/points.module#PointsModule'
        },
        {
            path: 'email',
            loadChildren: './email/email.module#EmailModule'
        }, {
            path: 'components',
            loadChildren: './components/components.module#ComponentsModule'
        }, {
            path: 'icons',
            loadChildren: './icons/icons.module#IconsModule'
        }, {
            path: 'cards',
            loadChildren: './cards/cards.module#CardsModule'
        }, {
            path: 'forms',
            loadChildren: './form/form.module#FormModule'
        }, {
            path: 'tables',
            loadChildren: './tables/tables.module#TablesModule'
        }, {
            path: 'datatable',
            loadChildren: './datatable/datatable.module#DatatableModule'
        }, {
            path: 'charts',
            loadChildren: './charts/charts.module#ChartsModule'
        }, {
            path: 'maps',
            loadChildren: './maps/maps.module#MapsModule'
        }, {
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }, {
            path: 'taskboard',
            loadChildren: './taskboard/taskboard.module#TaskboardModule'
        }, {
            path: 'calendar',
            loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
        }, {
            path: 'media',
            loadChildren: './media/media.module#MediaModule'
        }, {
            path: 'widgets',
            loadChildren: './widgets/widgets.module#WidgetsModule'
        }, {
            path: 'social',
            loadChildren: './social/social.module#SocialModule'
        }, {
            path: 'docs',
            loadChildren: './docs/docs.module#DocsModule'
        }],
    canActivate: [AuthGuard]
}, {
    path: '',
    component: AuthLayoutComponent,
    children: [
        {
            path: 'authentication',
            loadChildren: './authentication/authentication.module#AuthenticationModule'
        }, {
            path: 'error',
            loadChildren: './error/error.module#ErrorModule'
        }, {
            path: 'landing',
            loadChildren: './landing/landing.module#LandingModule'
        }]
}, {
    path: '**',
    redirectTo: 'error/404'
}];

