import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SidebarModule} from 'ng-sidebar';

import {AppRoutes} from './app.routing';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {SharedModule} from './shared/shared.module';
import {RestangularModule} from 'ngx-restangular';
import {AgmCoreModule} from '@agm/core';
import {google} from 'google-maps';
import {LoadingModule} from 'ngx-loading';
import {AuthGuard} from './_guards/auth.guard';

declare const google : google;

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function RestangularConfigFactory (RestangularProvider) {
    // RestangularProvider.setBaseUrl('http://vt.test/v1');
    RestangularProvider.setBaseUrl('http://app.y-travel.net/v1');
    // RestangularProvider.setBaseUrl('http://vt.kartisim.co.il/v1');

    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data) {
            console.log(url, data.data);
        }

        if (data && data.data == null && operation == 'getList'){
            return [];
        }

        return data.data;

    });
}

@NgModule({
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        AuthLayoutComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        HttpClientModule,
        LoadingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        NgbModule.forRoot(),
        SidebarModule.forRoot(),
        RestangularModule.forRoot(RestangularConfigFactory),
        AgmCoreModule.forRoot({apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"]})
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
